package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.laboratorioDAO.modification;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.enums.ArtistaEnum;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.laboratorioBC.TesteArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.util.Atualizador;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.util.Verificador;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteArtistaDAOModification extends TesteArtistaBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteArtistaDAOModification(ArtistaEnum artista) {
		super(artista);
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Metodo responsavel por realizar um INSERT na base de Dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso não esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(artista.getId() > 0);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar(ArtistaBC.getInstance().findById(artista.getId()), artista);
	}
	
	/**
	 * Metodo responsavel por atualizar um artista completo
	 */
	@Test
	public void updateArtistaCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Atualiza Atributos
		Atualizador.atualizar(artista, "U-");
		
		//Atualiza o objeto no BD
		ArtistaBC.getInstance().update(artista);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Verificador.verificar(ArtistaBC.getInstance().findById(artista.getId()), artista);
	}
	
	/**
	 * Metodo responsavel por deletar um artista completo
	 */
	@Test
	public void deletarArtistaCompleto(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();

		//Remove o objeto do BD
		ArtistaBC.getInstance().delete(artista);
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(ArtistaBC.getInstance().findById(artista.getId()));
	}
}
